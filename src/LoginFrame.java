
import java.awt.Dimension;
import java.awt.Toolkit;

public class LoginFrame extends javax.swing.JFrame {
    AnimPane ap;
    LogInPane lp;
    SignIn si;
    public LoginFrame() {
        initComponents();
//        this.setSize(615,520);
        ap=new AnimPane();
        ap.setBounds(0,0,320,520);
        
        si=new SignIn(this);
        si.setBounds(320,0,320,520);
        lp=new LogInPane(this);
        add(ap);
        add(lp);
        add(si);
        Dimension dimension=Toolkit.getDefaultToolkit().getScreenSize();
        setLocation(dimension.width/2-getWidth()/2,dimension.height/2-getHeight()/2);
    }    
    public void startLeftAnimation(){
        new Thread(new Runnable(){
            @Override
            public void run(){
                //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                add(lp);
                lp.setBounds(0,0,320,520);
                while(ap.getX()<307){
                    ap.setBounds(ap.getX()+2,ap.getY(),ap.getWidth(),ap.getHeight());
//                    lp.setBounds(lp.getX()+2,lp.getY(),lp.getWidth(),lp.getHeight());
//                    si.setBounds(si.getX()+2,si.getY(),si.getWidth(),si.getHeight());
                    try{
                        Thread.sleep(1);
                    }
                    catch(InterruptedException i){}
                }             
            }
        }).start();
       remove(si);
       ap.getPane().setVisible(true);
    }
    public void srartRightAnimation(){
        new Thread(new Runnable(){
            public void run(){
                add(si);
                si.setBounds(320,0,320,520);
                //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                while(ap.getX()>0){
                    ap.setBounds(ap.getX()-2,ap.getY(),ap.getWidth(),ap.getHeight());
//                    lp.setBounds(lp.getX()-2,lp.getY(),lp.getWidth(),lp.getHeight());
//                    si.setBounds(si.getX()-2,si.getY(),si.getWidth(),si.getHeight());
                    try{
                        Thread.sleep(1);
                    }
                    catch(InterruptedException i){}
                }             
            }
        }).start();
        remove(lp);
        ap.getPane().setVisible(false);
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);
        setResizable(false);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 630, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 520, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    
    public static void main(String args[]) {

        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new LoginFrame().setVisible(true);
            }
        });
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new LoginFrame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
