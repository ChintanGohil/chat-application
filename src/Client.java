import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.Scanner;
import java.util.*;

public class Client{
    public static void main(String[] args) {
        try{
            Socket socket=new Socket("localhost",5000);
            // socket.setSoTimeout(5000);
            BufferedReader input=new BufferedReader(new InputStreamReader(socket.getInputStream()));
            PrintWriter output=new PrintWriter(socket.getOutputStream(),true);
            Scanner scanner=new Scanner(System.in);
            String echoString;
            System.out.println("Enter your name : ");
            output.println(scanner.nextLine());
            new Send(socket).start();
//            new Echoer(socket).start();
        }catch(SocketTimeoutException e){
            System.out.println("Issue : "+e.getMessage());
        }catch(IOException e){
            System.out.println("Issue : "+e.getMessage());
        }
    }
}