
import java.io.IOException;
import java.net.ServerSocket;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.*;

public class Server{
	public static void main(String[] args) {
		HashMap<String,Socket> hm=new HashMap<>();
		try{
			ServerSocket server=new ServerSocket(5000);
			while(true){
				Socket socket=server.accept();
		
				System.out.println("inside server");
				BufferedReader input=new BufferedReader(new InputStreamReader(socket.getInputStream()));
				hm.put(input.readLine(),socket);
				new ServerEchoer(socket,hm).start();

			}
		}catch(IOException e){
			System.out.println("Issue : "+e.getMessage());
		}
	}
}